#pragma once
#include<string>
using namespace std;
class Validator
{
public:
	Validator();
	~Validator();
	static bool isPasswordValid(string);//check if the password is valid
	static bool isUsernameValid(string);//check if the username is valid
};

