#pragma once

#include "TriviaServer.h"

class Question;
class Game;
class User;

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool isUserExist(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
	void clearTable();

private:
	//static int callbackCount(void*, int, char**, char**);
	//static int callbackQuestions(void*, int, char**, char**);
	//static int callbackBestScores(void*, int, char**, char**);
	//static int callbackPersonalStatus(void*, int, char**, char**);
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	sqlite3* db;
};

