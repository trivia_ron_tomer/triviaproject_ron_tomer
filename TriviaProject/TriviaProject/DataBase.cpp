#include "DataBase.h"

unordered_map<string, vector<string>> results;



DataBase::DataBase()
{
	int rc;
	char *zErrMsg = 0;
	
	rc = sqlite3_open("triviadb.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		throw exception("Can not open database!");
	}
}


DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserExist(string username)
{
	int rc, check;
	char *zErrMsg = 0;
	stringstream s;

	s << "select count(*) from t_users where username = \"" << username << "\";";
	rc = sqlite3_exec(this->db,s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return false;
	}
	check = atoi(results.find("count(*)")->second.at(0).c_str());
	this->clearTable();
	if (check)
		return true;
	return false;
}
bool DataBase::addNewUser(string username, string pw, string email)
{
	int rc;
	char *zErrMsg = 0;
	stringstream s;

	s << "insert into t_users(username,password,email) values(\"" << username << "\",\"" << pw << "\",\"" << email << "\");";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return false;
	}
	this->clearTable();
	return true;
}
bool DataBase::isUserAndPassMatch(string username, string pw)
{
	int rc, check;
	char *zErrMsg = 0;
	stringstream s;

	s << "select count(*) from t_users where username = \"" << username << "\" and password = \"" << pw << "\";";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	check = atoi(results.find("count(*)")->second.at(0).c_str());
	this->clearTable();
	if (check == 1)
		return true;
	return false;
}
vector<Question*> DataBase::initQuestions(int numOfQuestions)
{
	int rc;
	char *zErrMsg = 0;
	stringstream s;
	vector < Question* > questions;

	s << "select * from t_questions	order by RANDOM() limit " << numOfQuestions << ";";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return questions;
	}
	for (int i = 0; i < numOfQuestions; i++)
	{
		questions.push_back(new Question(atoi(results.find("question_id")->second.at(i).c_str()), results.find("question")->second.at(i), results.find("correct_ans")->second.at(i), results.find("ans2")->second.at(i), results.find("ans3")->second.at(i), results.find("ans4")->second.at(i)));
	}
	this->clearTable();
	return questions;
}
vector<string> DataBase::getBestScores()
{
	int rc,len;
	char *zErrMsg = 0;
	stringstream s;
	vector < string > scores;
	string msg;

	s << "select username, count(*) from t_players_answers where is_correct = 1 group by username order by count(*) desc limit 3;";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return scores;
	}
	for (int i = 0; i < results.find("username")->second.size(); i++)
	{
		len = results.find("username")->second.at(i).size();
		if (len < 10)
			msg += string("0");
		msg += std::to_string(len);
		msg += results.find("username")->second.at(i);
		len = std::stoi(results.find("count(*)")->second.at(i));
		if (len < 10)
			msg += string("00000");
		else if (len < 100)
			msg += string("0000");
		else if (len < 1000)
			msg += string("000");
		else if (len < 10000)
			msg += string("00");
		else if (len < 100000)
			msg += string("0");
		msg += results.find("count(*)")->second.at(i);
		scores.push_back(msg);
		msg = string("");
	}
	if (results.find("username")->second.size() == 2)
	{
		scores.push_back("06Vacant000000");
	}
	if (results.find("username")->second.size() == 1)
	{
		scores.push_back("06Vacant000000");
		scores.push_back("06Vacant000000");
	}
	if (results.find("username")->second.size() == 0)
	{
		scores.push_back("06Vacant000000");
		scores.push_back("06Vacant000000");
		scores.push_back("06Vacant000000");
	}
	this->clearTable();
	return scores;

}
vector<string> DataBase::getPersonalStatus(string username)
{
	int rc, len;
	double t;
	char *zErrMsg = 0;
	stringstream s;
	vector < string > status;
	string msg;
	bool flag = false;
	s << "select count(*) from (select game_id from t_players_answers where username = \"" << username << "\" group by game_id);";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		status.clear();
		status.push_back(string("0000"));
		status.push_back(string("000000"));
		status.push_back(string("000000"));
		status.push_back(string("0000"));
		return status;
	}
	len = std::stoi(results.find("count(*)")->second.at(0));
	if (len < 10)
		msg += string("000");
	else if (len < 100)
		msg += string("00");
	else if (len < 1000)
		msg += string("0");
	msg +=(results.find("count(*)")->second.at(0));
	if (std::atoi(results.find("count(*)")->second.at(0).c_str()) != 0)
	{
		flag = true;
	}
	status.push_back(msg);
	msg = string("");
	s.str("");
	s.clear();
	this->clearTable();
	s << "select count(*) from t_players_answers where username = \"" << username << "\" and is_correct = 1;";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		status.clear();
		status.push_back(string("0000"));
		status.push_back(string("000000"));
		status.push_back(string("000000"));
		status.push_back(string("0000"));
		return status;
	}
	len = stoi(results.find("count(*)")->second.at(0));
	if (len < 10)
		msg += string("00000");
	else if (len < 100)
		msg += string("0000");
	else if (len < 1000)
		msg += string("000");
	else if (len < 10000)
		msg += string("00");
	else if (len < 100000)
		msg += string("0");
	msg += (results.find("count(*)")->second.at(0));
	status.push_back(msg);
	msg = string("");
	s.str("");
	s.clear();
	this->clearTable();
	s << "select count(*) from t_players_answers where username = \"" << username << "\" and is_correct = 0;";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		status.clear();
		status.push_back(string("0000"));
		status.push_back(string("000000"));
		status.push_back(string("000000"));
		status.push_back(string("0000"));
		return status;
	}
	len = stoi(results.find("count(*)")->second.at(0));
	if (len < 10)
		msg += string("00000");
	else if (len < 100)
		msg += string("0000");
	else if (len < 1000)
		msg += string("000");
	else if (len < 10000)
		msg += string("00");
	else if (len < 100000)
		msg += string("0");
	msg += (results.find("count(*)")->second.at(0));
	status.push_back(msg);
	msg = string("");
	s.str("");
	s.clear();
	this->clearTable();
	if (!flag)
	{
		status.push_back(string("0000"));
		return status;
	}
	s << "select avg(answer_time) from t_players_answers where username = \"" << username << "\";";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		status.clear();
		status.push_back(string("0000"));
		status.push_back(string("000000"));
		status.push_back(string("000000"));
		status.push_back(string("0000"));
		return status;
	}
	msg = string("");
	t = std::stod(results.find("avg(answer_time)")->second.at(0));
	len = (int)t;
	if (len < 10)
	{
		msg += string("0");
	}
	msg += std::to_string(len);
	t = t - len;
	t = t * 100;
	len = (int)t;
	if (len<10)
		msg += string("0");
	msg += std::to_string(len);
	status.push_back(msg);
	s.str("");
	s.clear();
	this->clearTable();
	return status;
}
int DataBase::insertNewGame()
{
	int rc, check;
	char *zErrMsg = 0;
	stringstream s;
	vector < Question* > questions;

	s << "insert into t_games(status,start_time) values(0,datetime('now'));";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return -1;
	}
	s << "select last_insert_rowid();";
	rc = sqlite3_exec(this->db, s.str().c_str(), this->callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return -1;
	}
	check = atoi(results.find("last_insert_rowid()")->second.at(0).c_str());
	this->clearTable();
	return check;
}
bool DataBase::updateGameStatus(int id)
{
	int rc;
	char *zErrMsg = 0;
	stringstream s;
	vector < Question* > questions;

	s << "update t_games set status = 1, end_time = datetime('now') where game_id = " << id << ";";
	rc = sqlite3_exec(this->db, s.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return false;
	}
	this->clearTable();
	return true;
}

bool DataBase::addAnswerToPlayer(int game_id, string username, int question_id, string answer, bool isCorrect, int answerTime)
{
	int rc;
	int isRight = 0;
	char *zErrMsg = 0;
	stringstream s;
	vector < Question* > questions;

	if (isCorrect)
		isRight = 1;

	s << "insert into t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) values(" << game_id << ",\"" << username << "\"," << question_id << ",\"" << answer << "\"," << isRight << "," << answerTime << ");";
	rc = sqlite3_exec(this->db, s.str().c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		this->clearTable();
		return false;
	}
	this->clearTable();
	return true;
}


int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
//static int callbackCount(void* unused, int argc, char** argv, char**)
//{
//
//}
//static int callbackQuestions(void* unused, int argc, char** argv, char**)
//{
//
//}
//static int callbackBestScores(void* unused, int argc, char** argv, char**)
//{
//
//}
//static int callbackPersonalStatus(void* unused, int argc, char** argv, char**)
//{
//
//}