#include "Protocol.h"


Protocol::Protocol()
{
}


Protocol::~Protocol()
{
}

string Protocol::request114(bool isInRoom)
{
	if (isInRoom)
		return string("1141");
	return string("1140");
}

string Protocol::request110(int code, int questionsNo, int questionsTime)
{
	string msg("110");
	msg += std::to_string(code);
	if (code == 0)
	{
		msg += std::to_string(questionsNo);
		msg += std::to_string(questionsTime);
	}
	return msg;
}

string Protocol::request112()
{
	return string("1120");
}

string Protocol::request116()
{
	return string("116");
}

string Protocol::request118(Question* qst)
{
	string msg = string("118");
	if (qst == NULL)
		return msg += string("0");
	int length;
	length = qst->getQuestion().size();
	if (length < 10)
		msg += string("00");
	else if (length<100)
	{
		msg += string("0");
	}
	msg += std::to_string(length);
	msg += qst->getQuestion();
	for (int i = 0; i < 4; i++)
	{
		length = (qst->getAnswers())[i].size();
		if (length < 10)
			msg += string("00");
		else if (length<100)
		{
			msg += string("0");
		}
		msg += std::to_string(length);
		msg += (qst->getAnswers())[i];
	}
	return msg;
}

string Protocol::request121(vector<User*> users, map<string, int> results)
{
	string msg = string("121");
	int length = users.size();
	msg += std::to_string(length);
	for (int i = 0; i < users.size(); i++)
	{
		length = users[i]->getUsername().size();
		if (length < 10)
			msg += string("0");
		msg += std::to_string(length);
		msg += users[i]->getUsername();
		length = results[users[i]->getUsername()];
		if (length < 10)
			msg += string("0");
		msg += std::to_string(length);
	}
	return msg;
}

string Protocol::request120(int isRight)
{
	return (string("120") += std::to_string(isRight));
}

string Protocol::request102(int status)
{
	if (status == 1)
		return string("1020");
	else if (status == 0)
		return string("1022");
	return string("1021");
}

string Protocol::request104(int status)
{
	return (string("104") += std::to_string(status));
}

string Protocol::request106(map<int, Room*> rooms)
{
	std::map<int, Room*>::iterator it;
	string msg = "106";
	int len = rooms.size();
	if (len < 10)
		msg += string("000");
	else if (len < 100)
		msg += string("00");
	else if (len < 1000)
		msg += string("0");
	msg += std::to_string(len);
	if (len > 0)
	{
		for (it = rooms.begin(); it != rooms.end(); it++)
		{
			len = it->first;
			if (len < 10)
				msg += string("000");
			else if (len < 100)
				msg += string("00");
			else if (len < 1000)
				msg += string("0");
			msg += std::to_string(len);
			len = stoi(it->second->getName());
			if (len < 10)
				msg += string("0");
			msg += std::to_string(len);
			msg += it->second->getName();
		}
	}
	return msg;
}