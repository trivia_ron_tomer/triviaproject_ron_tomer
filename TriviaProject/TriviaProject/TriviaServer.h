﻿#pragma once
using namespace std;
#include <stdlib.h>
#include <time.h>
#include <random>
#include<iostream>
#include <WinSock2.h>
#include<map>
#include"User.h"
#include"sqlite3.h"
#include "Room.h"
#include "DataBase.h"
#include<mutex>
#include<queue>
#include "ReceivedMessage.h"
#include "Helper.h"
#include <Windows.h>
#include "Protocol.h"
#include <exception>
#include <thread>
#include<condition_variable>
#include "Validator.h"
#include <sstream>
#include <unordered_map>

#define TRIVIA_CLIENT_SIGN_IN 200
#define TRIVIA_CLIENT_SIGN_OUT 201
#define TRIVIA_SERVER_SIGN_IN 102
#define TRIVIA_CLIENT_SIGN_UP 203
#define TRIVIA_SERVER_SIGN_UP 104
#define TRIVIA_CLIENT_ROOM_LIST 205
#define TRIVIA_SERVER_ROOM_LIST 106
#define TRIVIA_CLIENT_ROOM_USERS 207
#define TRIVIA_SERVER_ROOM_USERS 108
#define TRIVIA_CLIENT_JOIN_ROOM 209
#define TRIVIA_SERVER_JOIN_ROOM 110
#define TRIVIA_CLIENT_LEAVE_ROOM 211
#define TRIVIA_SERVER_LEAVE_ROOM 112
#define TRIVIA_CLIENT_CREATE_ROOM 213
#define TRIVIA_SERVER_CREATE_ROOM 114
#define TRIVIA__CLIENT_CLOSE_ROOM 215
#define TRIVIA__SERVER_CLOSE_ROOM 116
#define TRIVIA_CLIENT_START_GAME 217
#define TRIVIA_SERVER_SEND_Q_4A 118
#define TRIVIA_CLIENT_SEND_A 219
#define TRIVIA_SERVER_CHECK_ANS 120
#define TRIVIA_SERVER_END_GAME 121
#define TRIVIA_CLIENT_LEAVE_GAME 222
#define TRIVIA_CLIENT_BEST_SCORES 223
#define TRIVIA_SERVER_BEST_SCORES 124
#define TRIVIA_CLIENT_PERSONAL_STATE 225
#define TRIVIA_SERVER_PERSONAL_STATE 126
#define TRIVIA_CLIENT_EXIT 299

static const int TRIVIA_ERROR_CODE = 0;
class ReceivedMessage;
class DataBase;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();

private:
	//vars
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase* _db;
	map<int, Room*> _roomsList;
	
	mutex _mtxReceivedMessages;
	queue<ReceivedMessage*> _queRcvMessages;
	condition_variable _msgQueueCondition;

	int _roomIdSequence;


	//functions
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(ReceivedMessage*);


	User* handleSignin(ReceivedMessage*);
	bool handleSignup(ReceivedMessage*);
	void handlesignout(ReceivedMessage*);


	void handleLeaveGame(ReceivedMessage*);
	void handleStartGame(ReceivedMessage*);
	void handlePlayerAnswer(ReceivedMessage*);


	bool handleCreateRoom(ReceivedMessage*);
	bool handleCloseRoom(ReceivedMessage*);
	bool handleJoinRoom(ReceivedMessage*);
	bool handleLeaveRoom(ReceivedMessage*);
	void handleGetUserInRoom(ReceivedMessage*);
	void handleGetRooms(ReceivedMessage*);

	void handleGetBestScores(ReceivedMessage*);
	void handleGetPersonalStatus(ReceivedMessage*);

	void handleReceivedMessage();
	void addReceivedMessage(ReceivedMessage*);
	ReceivedMessage* buildReceivedMessage(SOCKET, int);
	
	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
};

