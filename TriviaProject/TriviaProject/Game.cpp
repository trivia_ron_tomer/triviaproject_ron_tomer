#include "Game.h"


Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	bool flag = this->insertGameToDB();
	if (flag)
	{
		this->_players = players;
		this->_questions_no = questionsNo;
		this->initQuestionsFromDB();
		for (int i = 0; i < this->_players.size(); i++)
		{
			this->_results.insert(std::pair<string, int>(this->_players[i]->getUsername(), 0));
			this->_players[i]->setGame(this);

		}
	}

}


Game::~Game()
{
	Question* qst;
	User* usr;
	int i = 0;
	for (i = _questions.size()-1; i >= 0; i--)
	{
		qst = _questions[i];
		_questions.pop_back();
		delete qst; //Retest
	}
	for (i = this->_players.size() - 1; i >= 0; i--)
	{
		usr = this->_players[i];
		_players.pop_back();
	}
}

void Game::sendFirstQuestion()
{
	this->sendQuestionsToAllUsers();
}
void Game::handleFinishGame()
{
	this->_db.updateGameStatus(this->getID());
	string msg = Protocol::request121(this->_players, this->_results);
	for (int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(msg);
		}
		catch (exception& e)
		{
			TRACE("failed to send to user end game message");
		}
		this->_players[i]->setGame(nullptr);
	}
}
bool Game::handleNextTurn()
{
	int usrsNum = this->_players.size();
	if (usrsNum == 0)
	{
		this->handleFinishGame();
		return false;
	}
	if (this->_currentTurnAnswers == this->_players.size())
	{
		if (this->_currQuestionIndex == this->_questions_no-1)//checkit
		{
			this->handleFinishGame();
			return false;
		}
		else
		{
			this->_currQuestionIndex++;
			this->sendQuestionsToAllUsers();
		}
	}
	return true;

}
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	answerNo = answerNo - 1;
	int isRight=0;
	string answer = string("");
	if (answerNo != 4)
	{
		answer = this->_questions[this->_currQuestionIndex]->getAnswers()[answerNo];
	}
	std::map<string, int>::iterator it;
	this->_currentTurnAnswers++;
	if (this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex() == answerNo)
	{
		it = this->_results.find(user->getUsername());
		if (it != this->_results.end())
		{
			it->second = this->_results[user->getUsername()] + 1;
			isRight = 1;
		}
	}
	this->_db.addAnswerToPlayer(this->getID(), user->getUsername(), this->_currQuestionIndex,answer , isRight == 1, time);
	user->send(Protocol::request120(isRight));
	return (this->handleNextTurn());
}
bool Game::leaveGame(User* currUser)
{
	for (int i = 0; i < this->_players.size(); i++)
	{
		if (this->_players[i] == currUser)//this->_players[i]->getUsername().compare(currUser->getUsername()) != 0
		{
			this->_players.erase(this->_players.begin() + i);
			return this->handleNextTurn();
		}
	}
	return true; //Get back to later. Make sure of the return value if did not enter loop
}
int Game::getID()
{
	return this->_id;
}

bool Game::insertGameToDB()
{
	int check = this->_db.insertNewGame();
	if (check != -1)
	{
		this->_id = check;
		return true;
	}
	return false;
}
void Game::initQuestionsFromDB()
{
	this->_questions = this->_db.initQuestions(this->_questions_no);
}
void Game::sendQuestionsToAllUsers()
{
	string msg = Protocol::request118(this->_questions[this->_currQuestionIndex]);
	this->_currentTurnAnswers = 0; 
	for (int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(msg);
		}
		catch (exception& e)//check it
		{
			this->_players[0]->send(Protocol::request118(NULL));
		}
	}
}
