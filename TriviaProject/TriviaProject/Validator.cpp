#include "Validator.h"


Validator::Validator()
{
}


Validator::~Validator()
{
}

bool Validator::isPasswordValid(string s)//check if the password is valid
{
	int c1=0, c2=0, c3=0, c4=0, c5=0;
	c1 = s.length();//check the password length
	if (c1 > 3)
		c1 = 1;
	c2 = s.find(' ');//check if the password contains ' '
	if (c2 == string::npos)
		c2 = 1;
	else
		c2 = 0;
	for (int i = 0; i < s.length(); i++)
	{
		if (s[i] > 47 && s[i] < 57)//check if the password contains number
			c3 = 1;
		else if (s[i] > 96 && s[i] < 123)//check if the password contains lowercase letter
			c4 = 1;
		else if (s[i] > 64 && s[i] < 91)//check if the password contains uppercase letter
			c5 = 1;
	}
	if (c1*c2*c3*c4*c5 == 1)//all the conditions are true
		return true;
	return false;
}

bool Validator::isUsernameValid(string s)//check if the username is valid
{
	if (s.length() !=0)//check if the username is not empty
	{
		if (s.find(' ') == string::npos)//check if the username contains ' '
		{
			if ((s[0] > 96 && s[0] < 123) || (s[0] > 64 && s[0] < 91))//check if the username start with letter
				return true;
		}
	}
	return false;
}