#include <iostream>
#include "Validator.h"
#include "TriviaServer.h"
#include "WSAInitializer.h"
#pragma comment(lib, "Ws2_32.lib")

int main()
{
	try
	{
		WSAInitializer wsa_init;
		TriviaServer server;
		server.serve();

	}
	catch (...){ cout << "Error" << endl; }

	return 0;
}