#include "ReceivedMessage.h"


ReceivedMessage::ReceivedMessage(SOCKET socket, int code)
{
	this->_sock = socket;
	this->_messageCode = code;
}

ReceivedMessage::ReceivedMessage(SOCKET socket, int code, vector<string> vals)
{
	this->_messageCode = code;
	this->_sock = socket;
	this->_values = vals;
}

ReceivedMessage::~ReceivedMessage()
{
}

SOCKET ReceivedMessage::getSock()
{
	return this->_sock;
}

User* ReceivedMessage::getUser()
{
	return this->_user;
}

void ReceivedMessage::setUser(User* user)
{
	this->_user = user;
}

int ReceivedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& ReceivedMessage::getValues()
{
	return this->_values;
}

