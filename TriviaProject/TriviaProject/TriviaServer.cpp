﻿#include "TriviaServer.h"

static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;

TriviaServer::TriviaServer() 
{
	this->_db = new DataBase();
	cout << "Creating socket..." << endl;
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception("Error while creating socket");
}


TriviaServer::~TriviaServer()
{
	cout << "Closing socket..." << endl;
	for (std::map<int, Room*>::iterator it = this->_roomsList.begin(); it != this->_roomsList.end();it++)
	{
		this->_roomsList.erase(it);
		delete it->second;
	}
	for (std::map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		this->_connectedUsers.erase(it);
		delete it->second; 
	}

	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
	delete this->_db;
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	cout << "Binding..." << endl;

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening..." << endl;
}

void TriviaServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleReceivedMessage, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		this->accept();
	}
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	cout << "Accepting client..." << endl;

	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	ReceivedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(client_socket);

		while ((msgCode != TRIVIA_ERROR_CODE) && (msgCode != TRIVIA_CLIENT_EXIT))
		{
			currRcvMsg = buildReceivedMessage(client_socket, msgCode);
			addReceivedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = buildReceivedMessage(client_socket, 299);
		addReceivedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was caught in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildReceivedMessage(client_socket, 299);
		addReceivedMessage(currRcvMsg);
	}
	closesocket(client_socket);
}

void TriviaServer::safeDeleteUser(ReceivedMessage* rcvMsg)
{
	try
	{
		SOCKET userSocket = rcvMsg->getSock();
		handlesignout(rcvMsg);
		::closesocket(userSocket);
		cout << "Deleting user..." << endl;

	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}

}

User* TriviaServer::getUserByName(string name)
{
	for (std::map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->second->getUsername().compare(name) == 0)
			return it->second;
	}
	return nullptr;
}

Room* TriviaServer::getRoomById(int id)
{
	for (std::map<int, Room*>::iterator it = this->_roomsList.begin(); it != this->_roomsList.end(); it++)
	{
		if (it->second->getId() == id)
			return it->second;
	}
	return nullptr;
}

User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	for (std::map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->first == client_socket)
			return it->second;
	}
	return nullptr;
}

void TriviaServer::handleReceivedMessage()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	ReceivedMessage* currMessage =NULL;
	while (true)
	{
		try
			{
			unique_lock<mutex> lck(_mtxReceivedMessages);

			// Wait for clients to enter the queue.
			if (this->_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (this->_queRcvMessages.empty())
				continue;

			currMessage = this->_queRcvMessages.front();
			this->_queRcvMessages.pop();
			lck.unlock();

			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();

			if (msgCode == TRIVIA_CLIENT_SIGN_IN) //Login msgCode
			{
				this->handleSignin(currMessage);
				cout << "Handling sign in." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_SIGN_OUT)//Logout msgCode
			{
				this->handlesignout(currMessage);
				cout << "Handling sign out." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_SIGN_UP)
			{
				this->handleSignup(currMessage);
				cout << "Handling sign up." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_LEAVE_GAME)
			{
				this->handleLeaveGame(currMessage);
				cout << "Handling leave game." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_START_GAME)
			{
				this->handleStartGame(currMessage);
				cout << "Handling start game." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_SEND_A)
			{
				this->handlePlayerAnswer(currMessage);
				cout << "Handling player answer." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_CREATE_ROOM)
			{
				this->handleCreateRoom(currMessage);
				cout << "Handling create room." << endl;
			}
			else if (msgCode == TRIVIA__CLIENT_CLOSE_ROOM)
			{
				this->handleCloseRoom(currMessage);
				cout << "Handling close room." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_JOIN_ROOM)
			{
				this->handleJoinRoom(currMessage);
				cout << "Handling join room." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_LEAVE_ROOM)
			{
				this->handleLeaveRoom(currMessage);
				cout << "Handling leave room." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_ROOM_USERS)
			{
				this->handleGetUserInRoom(currMessage);
				cout << "Handling get user in room." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_ROOM_LIST)
			{
				this->handleGetRooms(currMessage);
				cout << "Handling get rooms." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_BEST_SCORES)
			{
				this->handleGetBestScores(currMessage);
				cout << "Handling get best scores." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_PERSONAL_STATE)
			{
				this->handleGetPersonalStatus(currMessage);
				cout << "Handling get personal status." << endl;
			}
			else if (msgCode == TRIVIA_CLIENT_EXIT)
			{
				this->safeDeleteUser(currMessage);
			}
			else
			{
				safeDeleteUser(currMessage);
			}
			delete currMessage;
		}
		catch (...)
		{
			if (currMessage!=NULL)
				safeDeleteUser(currMessage);
		}
	}
}

User* TriviaServer::handleSignin(ReceivedMessage* msg)
{
	User* newUser;
	if (this->_db->isUserAndPassMatch(msg->getValues()[0], msg->getValues()[1]))
	{
		if (this->getUserByName(msg->getValues()[0]) != nullptr)
		{
			this->getUserByName(msg->getValues()[0])->send(Protocol::request102(0));
		}
		else
		{
			newUser = new User(msg->getValues()[0], msg->getSock());
			newUser->send(Protocol::request102(1));
			this->_connectedUsers.insert(std::pair<SOCKET, User*>(msg->getSock(), newUser));
			return newUser;
		}

	}
	Helper::sendData(msg->getSock(), Protocol::request102(-1));
	return nullptr;

}
bool TriviaServer::handleSignup(ReceivedMessage* msg)
{
	string user, pass, email;
	user = msg->getValues()[0];
	pass = msg->getValues()[1];
	email = msg->getValues()[2];
	if (!Validator::isPasswordValid(pass))
	{
		Helper::sendData(msg->getSock(), Protocol::request104(1));
		return false;
	}
	if (!Validator::isUsernameValid(user))
	{
		Helper::sendData(msg->getSock(), Protocol::request104(3));
		return false;
	}
	if (this->_db->isUserExist(user))
	{
		Helper::sendData(msg->getSock(),Protocol::request104(2));
		return false;
	}
	if (this->_db->addNewUser(user, pass, email))
	{
		Helper::sendData(msg->getSock(), Protocol::request104(0));
		return true;
	}
	Helper::sendData(msg->getSock(), Protocol::request104(4));
	return false;

}
void TriviaServer::handlesignout(ReceivedMessage* msg)
{
	std::map<SOCKET, User*>::iterator it;
	if (this->getUserBySocket(msg->getSock()) != NULL)
	{
		it = this->_connectedUsers.find(msg->getSock());
		this->handleCloseRoom(msg);
		this->handleLeaveRoom(msg);
		this->handleLeaveGame(msg);
		this->_connectedUsers.erase(it);
	}
}


void TriviaServer::handleLeaveGame(ReceivedMessage* msg)
{
	if (this->getUserBySocket(msg->getSock())->leaveGame())
	{
		delete this->getUserBySocket(msg->getSock())->getGame();
	}
}
void TriviaServer::handleStartGame(ReceivedMessage* msg)
{
	bool isSuccess = true;
	Game* newGame = NULL;
	std::map<int, Room*>::iterator it = this->_roomsList.find(this->getUserBySocket(msg->getSock())->getRoom()->getId());
	try
	{
		newGame = new Game(this->getUserBySocket(msg->getSock())->getRoom()->getUsers(), this->getUserBySocket(msg->getSock())->getRoom()->getQuestionNo(), *(this->_db));
	}
	catch (exception &e)
	{
		cout << "Error creating game! Error: " << e.what() << endl;
		this->getUserBySocket(msg->getSock())->send(Protocol::request118(NULL));//check it
		isSuccess = false;
	}
	if (isSuccess)
	{
		this->_roomsList.erase(it);
		this->getUserBySocket(msg->getSock())->setGame(newGame);
		newGame->sendFirstQuestion();
	}
}
void TriviaServer::handlePlayerAnswer(ReceivedMessage* rcvMsg)
{
	bool isActive = false;
	Game* game = this->getUserBySocket(rcvMsg->getSock())->getGame();
	if (game != NULL)
	{
		isActive = game->handleAnswerFromUser(this->getUserBySocket(rcvMsg->getSock()), stoi(rcvMsg->getValues()[0]), stoi(rcvMsg->getValues()[1]));
		if (!isActive)
		{
			delete game;
		}
	}

}


bool TriviaServer::handleCreateRoom(ReceivedMessage* rcvMsg)
{
	bool isSuccessful = false;
	User* user = this->getUserBySocket(rcvMsg->getSock());
	if (user != NULL)
	{
		this->_roomIdSequence++;
		isSuccessful = this->getUserBySocket(rcvMsg->getSock())->createRoom(stoi(rcvMsg->getValues()[1]), rcvMsg->getValues()[0], stoi(rcvMsg->getValues()[2]), stoi(rcvMsg->getValues()[3]), this->_roomIdSequence);
		if (isSuccessful)
		{
			this->_roomsList.insert(std::pair<int, Room*>(this->_roomIdSequence, this->getUserBySocket(rcvMsg->getSock())->getRoom()));
		}
		else
		{
			this->_roomIdSequence--;
		}
	}
	return isSuccessful;
}
bool TriviaServer::handleCloseRoom(ReceivedMessage* rcvMsg)
{
	int isSuccessful;
	Room* room = this->getUserBySocket(rcvMsg->getSock())->getRoom();
	std::map<int, Room*>::iterator it;
	if (room == NULL)
		return false;
	int roomID = room->getId();
	isSuccessful = this->getUserBySocket(rcvMsg->getSock())->closeRoom();
	if (isSuccessful != -1)
	{
		it = this->_roomsList.find(roomID);
		this->_roomsList.erase(it);
		delete it->second;
	}
	return (isSuccessful != -1);
}

bool TriviaServer::handleJoinRoom(ReceivedMessage* rcvMsg)
{
	bool isSuccessful = false;
	User* user = this->getUserBySocket(rcvMsg->getSock());
	Room* room = nullptr;
	int roomId = 0;
	if (user == NULL)
		return isSuccessful;
	roomId = stoi(rcvMsg->getValues()[0]);
	room = this->getRoomById(roomId);
	if (room == nullptr)
	{
		this->getUserBySocket(rcvMsg->getSock())->send(Protocol::request110(2, 0, 0));
		return isSuccessful;
	}
	else
	{
		isSuccessful = this->getUserBySocket(rcvMsg->getSock())->joinRoom(room);
		return isSuccessful;
	}
}
bool TriviaServer::handleLeaveRoom(ReceivedMessage* rcvMsg)
{
	User* user = this->getUserBySocket(rcvMsg->getSock());
	Room* room;
	if (user == NULL)
		return false;
	room = user->getRoom();
	if (room == nullptr)
		return false;
	user->leaveRoom();
	return true;
}
void TriviaServer::handleGetUserInRoom(ReceivedMessage* rcvMsg)
{
	User* user = this->getUserBySocket(rcvMsg->getSock());
	int roomId = stoi(rcvMsg->getValues()[0]);
	Room* room = user->getRoom();
	if (room == nullptr)
		user->send("1080");
	else
		user->send(room->getUsersListMessage());

}
void TriviaServer::handleGetRooms(ReceivedMessage* rcvMsg)
{
	this->getUserBySocket(rcvMsg->getSock())->send(Protocol::request106(this->_roomsList));
}

void TriviaServer::handleGetBestScores(ReceivedMessage* rcvMsg)
{
	vector<string> msgs = this->_db->getBestScores();
	string msg = string("124");
	for (int i = 0; i < 3; i++)
	{
		msg += msgs[i];
	}
	this->getUserBySocket(rcvMsg->getSock())->send(msg);
}
void TriviaServer::handleGetPersonalStatus(ReceivedMessage* rcvMsg)
{
	vector<string> msgs = this->_db->getPersonalStatus(getUserBySocket(rcvMsg->getSock())->getUsername());
	string msg = string("126");
	for (int i = 0; i < 4; i++)
	{
		msg += msgs[i];
	}
	this->getUserBySocket(rcvMsg->getSock())->send(msg);
}

void TriviaServer::addReceivedMessage(ReceivedMessage* rcvMsg)
{
	this->_mtxReceivedMessages.lock();
	this->_queRcvMessages.push(rcvMsg);
	this->_mtxReceivedMessages.unlock();
	this->_msgQueueCondition.notify_all();

}
ReceivedMessage* TriviaServer::buildReceivedMessage(SOCKET client_socket, int msgCode)
{
	vector<string> vals;
	int begin = 0;
	if (msgCode == TRIVIA_CLIENT_SIGN_IN) //Login msgCode
	{
		for (int i = 0; i < 2; i++)
		{
			begin = Helper::getIntPartFromSocket(client_socket, 2);
			vals.push_back(Helper::getStringPartFromSocket(client_socket, begin));
		}
	}
	else if (msgCode == TRIVIA_CLIENT_SIGN_UP)
	{
		for (int i = 0; i < 3; i++)
		{
			begin = Helper::getIntPartFromSocket(client_socket, 2);
			vals.push_back(Helper::getStringPartFromSocket(client_socket, begin));
		}
	}
	else if (msgCode == TRIVIA_CLIENT_SEND_A)
	{
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 2));
	}
	else if (msgCode == TRIVIA_CLIENT_CREATE_ROOM)
	{
		begin = Helper::getIntPartFromSocket(client_socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(client_socket, begin));
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 2));
	}
	else if (msgCode == TRIVIA_CLIENT_JOIN_ROOM)
	{
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 4));
	}
	else if (msgCode == TRIVIA_CLIENT_ROOM_USERS)
	{
		vals.push_back(Helper::getStringPartFromSocket(client_socket, 4));
	}
	return new ReceivedMessage(client_socket, msgCode, vals);
}