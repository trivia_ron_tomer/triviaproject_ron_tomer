#include "Question.h"


Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int randomNum;
	srand(time(NULL));
	randomNum = rand() % 4;
	this->_correctAnswerIndex = randomNum;
	this->_question = question;
	this->_id = id;
	this->_answers[0] = correctAnswer;
	this->_answers[1] = answer2;
	this->_answers[2] = answer3;
	this->_answers[3] = answer4;
	if (randomNum != 0)
	{
		answer2 = this->_answers[randomNum];
		this->_answers[randomNum] = correctAnswer;
		this->_answers[0] = answer2;
	}
}


Question::~Question()
{
}

string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}