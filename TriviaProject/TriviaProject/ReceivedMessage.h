#pragma once
#include "TriviaServer.h"
#include "User.h"
#include <string>

using namespace std;

class ReceivedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
	

public:
	ReceivedMessage(SOCKET, int);
	ReceivedMessage(SOCKET, int, vector<string>);
	~ReceivedMessage();
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();


};

