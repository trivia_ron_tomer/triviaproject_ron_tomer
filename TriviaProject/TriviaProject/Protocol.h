#pragma once

#include <string>
#include "Question.h"

using namespace std;


class Protocol
{
public:
	Protocol();
	~Protocol();
	static string request114(bool);
	static string request110(int,int,int);
	static string request112();
	static string request116();
	static string request118(Question*);
	static string request121(vector<User*>, map<string, int>);
	static string request120(int);
	static string request102(int);
	static string request104(int);
	static string request106(map<int, Room*>);
};

