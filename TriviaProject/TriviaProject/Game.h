#pragma once
#include "TriviaServer.h"
#include "Question.h"

class Player;
class Question;
class User;
class Room;
class DataBase;

class Game
{
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map <string, int> _results;
	int _currentTurnAnswers;
	int _id;


	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionsToAllUsers();

	//***********//

public:
	Game(const vector<User*>&,int,DataBase&);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User*, int, int);
	bool leaveGame(User*);
	int getID();

	
};

