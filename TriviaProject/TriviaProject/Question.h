#pragma once
#include "TriviaServer.h"
#include <string>
class Question
{
private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;

public:
	Question(int, string, string, string, string, string);
	~Question();
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();
};

