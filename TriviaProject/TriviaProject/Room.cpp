#include "Room.h"

class User;
Room::Room(int maxUsers, User* admin, string name, int questionTime, int questionNum, int id)
{
	this->_maxUsers = maxUsers;
	this->_admin = admin;
	this->_name = name;
	this->_questionNo = questionNum;
	this->_questionTime = questionTime;
	this->_id = id;
	this->_users.push_back(admin);
}


Room::~Room()
{
}

string Room::getUsersAsString(vector<User*> userList, User* execludeUser)
{
	string retVal("");
	for (int i = 0; i < userList.size(); i++)
	{
		if (execludeUser != userList[i])//execludeUser->getUsername().compare(userList[i]->getUsername()) != 0
		{
			retVal += userList[i]->getUsername();
			retVal += " ";
		}
	}
	return retVal;
}

string Room::getUsersListMessage()
{
	string msg;
	int numOfUsers = this->_users.size();
	int length;
	msg += "108";
	msg += std::to_string(numOfUsers);
	if (numOfUsers == 0)
		return msg;
	for (int i = 0; i < this->_users.size(); i++)
	{
		length = this->_users[i]->getUsername().size();
		if (length < 10);
			msg += string("0");
		msg += std::to_string(this->_users[i]->getUsername().size());
		msg += this->_users[i]->getUsername();
	}
	return msg;

}

int Room::getId()
{
		return this->_id;
}

int Room::getQuestionNo()
{
	return this->_questionNo;
}

string Room::getName()
{
	return this->_name;
}

vector<User*> Room::getUsers()
{
	return this->_users;
}

void Room::sendMessage(User* execludeUser, string message)
{
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != execludeUser)//this->_users[i]->getUsername().compare(execludeUser->getUsername()) != 0
		{
			try
			{
				this->_users[i]->send(message);
			}
			catch (exception& e)
			{
				cout << (e.what()) << endl;
			}
		}
	}
}

void Room::sendMessage(string message)
{
	this->sendMessage(NULL, message);
}

bool Room::joinRoom(User* user)
{
	if (this->_users.size() == this->_maxUsers)
	{
		user->send(Protocol::request110(1, 0, 0));
		return false;
	}
	this->_users.push_back(user);
	user->send(Protocol::request110(0, this->_questionNo, this->_questionTime));
	this->sendMessage(this->getUsersListMessage());
	return true;
}
void Room::leaveRoom(User* user)
{

	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == user) //this->_users[i]->getUsername().compare(user->getUsername()) == 0
		{
			this->_users.erase(this->_users.begin() + i);
			user->setGame(nullptr);
			user->send(Protocol::request112());
			sendMessage(this->getUsersListMessage());
		}
	}

}
int Room::closeRoom(User* user)
{
	if (user != this->_admin) // this->_admin->getUsername().compare(user->getUsername()) != 0
		return -1;
	this->sendMessage(Protocol::request116());
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != this->_admin) //this->_users[i]->getUsername().compare(this->_admin->getUsername()) != 0
		{
			this->_users[i]->clearRoom();
		}
	}
	return this->_id;
}