#include "User.h"


User::User(string name, SOCKET socket)
{
	this->_username = name;
	this->_sock = socket;
	this->_currRoom = nullptr;
	this->_currGame = nullptr;

}


User::~User()
{

}

string User::getUsername()
{
	return this->_username;
}

void User::send(string msg)
{
	Helper::sendData(this->_sock, msg);
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* game)
{
	this->_currGame = game;
	this->_currRoom = nullptr;
}

void User::clearRoom()
{

	this->_currRoom = nullptr;

}

bool User::createRoom(int maxUsers, string roomName, int questionsNo, int questionsTime, int roomId)
{
	bool isInRoom = (this->_currRoom != nullptr);
	if (isInRoom)
	{
		send(Protocol::request114(isInRoom));
		return false;
	}
	this->_currRoom = new Room(maxUsers, this, roomName, questionsTime, questionsNo, roomId);
	send(Protocol::request114(isInRoom));
	return true;
}

bool User::joinRoom(Room* newRoom)
{
	bool isInRoom = (this->_currRoom != nullptr);
	if (isInRoom)
		return false;
	if (newRoom == NULL)
	{
		this->send(Protocol::request110(2, 0, 0));
		return false;
	}
	if(newRoom->joinRoom(this))
		return true;
	return false;
}

void User::leaveRoom()
{
	bool isInRoom = (this->_currRoom != nullptr);
	if (isInRoom)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}

}

int User::closeRoom()
{
	int id = this->_currRoom->getId();
	if (this->_currRoom == nullptr)
		return -1;
	this->_currRoom->closeRoom(this);
	delete this->_currRoom;
	this->_currRoom = nullptr;
	return id;

}

bool User::leaveGame()
{
	bool isGameActive;
	bool isInGame = (this->_currGame != nullptr);
	if (isInGame)
	{
		isGameActive = this->_currGame->leaveGame(this);
		this->_currGame = nullptr;
		return isGameActive;
	}
	return false;
}

void User::clearGame()
{
	this->_currGame = nullptr;
}